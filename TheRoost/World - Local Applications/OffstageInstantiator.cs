using SecretHistories.Entities;
using SecretHistories.Spheres;
using SecretHistories.Infrastructure;


namespace Roost.World
{
    internal class OffstageInstantiator
    {
        public static void Enact()
        {
            AtTimeOfPower.TabletopSceneInit.Schedule(InstantiateOffstageSphere, PatchType.Postfix);
        }

        const string OFFSTAGE = "offstage";
        public static void InstantiateOffstageSphere()
        {
            DealersTable dealerstable = FucineRoot.Get().DealersTable;
            Sphere offstage = dealerstable.GetSphereById(OFFSTAGE);

            if (!offstage.IsValid())
                offstage = dealerstable.TryCreateOrRetrieveSphere(new SphereSpec(typeof(OffstageSphere), OFFSTAGE));

            offstage.gameObject.transform.position = UnityEngine.Vector3.up * 2000;
            offstage.gameObject.SetActive(false);
        }
    }
}
