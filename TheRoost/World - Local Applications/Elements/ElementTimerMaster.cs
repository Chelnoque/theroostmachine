using SecretHistories.Entities;
using SecretHistories.Fucine.DataImport;
using SecretHistories.Fucine;
using SecretHistories.Spheres;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using SecretHistories.UI;
using System.Xml;
using System.Xml.Linq;
using static UnityEngine.EventSystems.EventTrigger;
using Roost.World.Recipes.Entities;
using Roost.World.Recipes;
using SecretHistories.Commands;
using SecretHistories.Abstract;
using SecretHistories.Infrastructure.Persistence;
using SecretHistories.Enums;
using HarmonyLib;
using static UnityEngine.UI.Image;
using System.Reflection;

namespace Roost.World.Elements
{
    class TimerData : AbstractEntity<TimerData>
    {
        [FucineEverValue(DefaultValue = 60000)] public int Frequency { get; set; }
        [FucineList] public List<RefMorphDetails> MorphEffects { get; set; }
        [FucineAutoValue(DefaultValue = true)] public bool Repeat { get; set; }

        public TimerData() { }
        public TimerData(EntityData importDataForEntity, ContentImportLog log) : base(importDataForEntity, log) { }
        protected override void OnPostImportForSpecificEntity(ContentImportLog log, Compendium populatedCompendium) { }
    }

    public static class ElementTimerMaster
    {
        const string TIMERS = "timers";
        static Dictionary<ITokenPayload, Dictionary<string, int>> TimersCache = new();
        static Dictionary<ITokenPayload, Dictionary<string, bool>> TimersRepeatCache = new();

        public static TValue GetValueOrDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue defaultValue)
        {
            return dictionary.TryGetValue(key, out var value) ? value : defaultValue;
        }

        public static void Enact()
        {
            Machine.ClaimProperty<Element, Dictionary<string, TimerData>>(TIMERS);
            
            AtTimeOfPower.MenuSceneInit.Schedule(ClearCache, PatchType.Prefix);
            AtTimeOfPower.TabletopSceneInit.Schedule(ClearCache, PatchType.Prefix);

            Machine.Patch<Compendium>(
                original: nameof(Compendium.TryAddEntity),
                postfix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(GenerateTimerAspects))
            );

            Machine.Patch<ElementStackCreationCommand>(
                original: nameof(ElementStackCreationCommand.Execute),
                postfix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(LoadTimerCounts))
            );

            Machine.Patch<GamePersistenceProvider>(
                original: nameof(GamePersistenceProvider.Encaust),
                prefix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(PersistTimers))
            );

            Machine.Patch<ElementStack>(
                original: nameof(ElementStack.ExecuteHeartbeat),
                prefix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(UpdateTimers))
            );
            
            Machine.Patch(
                original: typeof(ElementStack).GetMethodInvariant("Retire", new System.Type[] { typeof(RetirementVFX) }),
                prefix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(ClearFromCache))
            );

            Machine.Patch<ElementStack>(
                original: nameof(ElementStack.ChangeTo),
                prefix: typeof(ElementTimerMaster).GetMethodInvariant(nameof(ClearFromCache))
            );
        }

        public static void ClearCache()
        {
            TimersCache.Clear();
        }

        public static void LoadTimerCounts(ITokenPayload __result)
        {
            if (__result is not ElementStack) return;
            try
            {
                var orderedAspects = __result.GetAspects(true).OrderBy(aspect => aspect.Key).ToList();

                foreach (var aspect in orderedAspects)
                {
                    Element element = Watchman.Get<Compendium>().GetEntityById<Element>(aspect.Key);
                    Dictionary<string, TimerData> timers = element.RetrieveProperty<Dictionary<string, TimerData>>(TIMERS);

                    if (timers == null) continue;

                    foreach (KeyValuePair<string, TimerData> entry in timers)
                    {
                        TimerData timer = entry.Value;
                        string timerCounterAspectId = TimerAspectId(element.Id, entry.Key);
                        string timerRepeatAspectId = TimerRepeatId(element.Id, entry.Key);

                        int currentCount = __result.Mutations.GetValueOrDefault(timerCounterAspectId, 0);

                        //Birdsong.Sing("Loading and caching timer ", timerCounterAspectId, "for payload ", __result.Id, ", count:", currentCount);
                        CacheTimerValue(__result, timerCounterAspectId, currentCount);

                        if(!timer.Repeat)
                        {
                            int repeatCount = __result.Mutations.GetValueOrDefault(timerRepeatAspectId, 0);
                            if (repeatCount > 0) CacheRepeatValue(__result, timerRepeatAspectId);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Birdsong.Sing("DEPERSIST TIMERS ERROR:", e.Message);
                Birdsong.Sing(e.StackTrace);
            }
        }

        public static void CacheTimerValue(ITokenPayload payload, string timerId, int count)
        {
            if (!TimersCache.ContainsKey(payload))
            {
                TimersCache[payload] = new Dictionary<string, int>();
            }
            TimersCache[payload][timerId] = count;
        }

        public static void CacheRepeatValue(ITokenPayload payload, string timerId)
        {
            if (!TimersRepeatCache.ContainsKey(payload))
            {
                TimersRepeatCache[payload] = new Dictionary<string, bool>();
            }
            TimersRepeatCache[payload][timerId] = true;
        }

        public static bool GetAlreadyExecuted(ITokenPayload payload, string timerId)
        {
            if (!TimersRepeatCache.ContainsKey(payload)) return false;
            return TimersRepeatCache[payload].GetValueOrDefault(timerId, false);
        }

        public static int GetTimerValue(ITokenPayload payload, string timerId)
        {
            if (!TimersCache.ContainsKey(payload)) return 0;
            return TimersCache[payload].GetValueOrDefault(timerId, 0);
        }

        public static void ClearFromCache(ElementStack __instance)
        {
            TimersCache.Remove(__instance);
            TimersRepeatCache.Remove(__instance);
        }

        public static void PersistTimers()
        {
            try
            {
                //Birdsong.Sing("Persisting Timers as mutations...");
                foreach (var cachedTimerValues in TimersCache)
                {
                    ITokenPayload payload = cachedTimerValues.Key;
                    Dictionary<string, int> timers = cachedTimerValues.Value;

                    //Birdsong.Sing("Persisting timers for payload:", payload.Id);

                    foreach (var timerEntry in timers)
                    {
                        //Birdsong.Sing("Mutating TimerId ", timerEntry.Key, "value: ", timerEntry.Value);
                        //payload.SetIllumination(timerEntry.Key, timerEntry.Value.ToString());
                        payload.SetMutation(timerEntry.Key, timerEntry.Value, false);

                        string prefix = payload.EntityId + ".timers.";
                        string timerId = timerEntry.Key.Substring(prefix.Length);

                        //Birdsong.Sing($"Computing timer repeat id based on aspect id. Element id={payload.EntityId} Aspect id={timerEntry.Key}, Repeat id={timerId}");

                        if(GetAlreadyExecuted(payload, timerId))
                        {
                            payload.SetMutation(timerId, 1, false);
                        }
                    }
                }
                //Birdsong.Sing("Persisted Timers!");
            }
            catch(Exception e)
            {
                Birdsong.Sing("PERSIST TIMERS ERROR:", e.Message);
                Birdsong.Sing(e.StackTrace);
            }

        }

        public static void GenerateTimerAspects(Compendium __instance, IEntityWithId entityToAdd, bool __result)
        {
            if (!__result) return;
            if (entityToAdd is not Element) return;
            Element element = (Element) entityToAdd;
            
            Dictionary<string, TimerData> timers = element.RetrieveProperty<Dictionary<string, TimerData>>(TIMERS);
            if(timers == null) return;

            foreach (KeyValuePair<string, TimerData> entry in timers)
            {
                Element timerCounterAspect = new(new EntityData(TimerAspectId(element.Id, entry.Key), new Hashtable()), ContentImportLog.Current)
                {
                    IsAspect = true,
                    IsHidden = true
                };
                __instance.TryAddEntity(timerCounterAspect);

                if(!entry.Value.Repeat)
                {
                    //Birdsong.Sing($"Timer {element.Id}-{entry.Key} is non repeatable. Generating aspect {TimerRepeatId(element.Id, entry.Key)}");
                    Element timerRepeatAspect = new(new EntityData(TimerRepeatId(element.Id, entry.Key), new Hashtable()), ContentImportLog.Current)
                    {
                        IsAspect = true,
                        IsHidden = true
                    };
                    __instance.TryAddEntity(timerRepeatAspect);
                }
            }
        }

        public static void UpdateTimers(ElementStack __instance, float seconds)
        {
            if(seconds == 0) return;

            Compendium compendium = Watchman.Get<Compendium>();

            var orderedAspects = __instance.GetAspects(true).OrderBy(aspect => aspect.Key).ToList();

            foreach (var aspect in orderedAspects)
            {
                Element element = Watchman.Get<Compendium>().GetEntityById<Element>(aspect.Key);
                Dictionary<string, TimerData> timers = element.RetrieveProperty<Dictionary<string, TimerData>>(TIMERS);

                if (timers == null) continue;

                foreach (KeyValuePair<string, TimerData> entry in timers)
                {
                    TimerData timer = entry.Value;
                    string timerCounterAspectId = TimerAspectId(element.Id, entry.Key);
                    string timerRepeatAspectId = TimerRepeatId(element.Id, entry.Key);

                    if (GetAlreadyExecuted(__instance, timerRepeatAspectId))
                    {
                        //Birdsong.Sing($"Timer {element.Id}-{entry.Key} already executed on this card. Won't run it again.");
                        continue;
                    }
                    int currentCount = GetTimerValue(__instance, timerCounterAspectId);
                    int newCount = currentCount + (int)(seconds * 1000);

                    // If the mutated amount + seconds * 1000 goes >= the timer frequency, remove the difference and trigger it.
                    if (newCount >= timer.Frequency)
                    {
                        CacheTimerValue(__instance, timerCounterAspectId, timer.Frequency - newCount);
                        //__instance.SetMutation(timerCounterAspectId, timer.Frequency - newCount, false);
                        RunTimer(__instance.Token, timerCounterAspectId, timer.MorphEffects);

                        if(!timer.Repeat)
                        {
                            //Birdsong.Sing($"Timer {element.Id}-{entry.Key} executed on this card, and is non repeatable. Won't run it again in the future.");
                            CacheRepeatValue(__instance, timerRepeatAspectId);
                        }
                    }
                    // Otherwise, cache the new count.
                    else
                    {
                        CacheTimerValue(__instance, timerCounterAspectId, newCount);
                        //__instance.SetMutation(timerCounterAspectId, newCount, false);
                    }
                }
            }
        }

        public static void RunTimer(Token token, string timerCounterAspect, List<RefMorphDetails> morphDetailsList)
        {
            foreach (RefMorphDetails morphDetails in morphDetailsList) {
                if (morphDetails.Execute(token, timerCounterAspect, 1, 1))
                    break;
                RecipeExecutionBuffer.ApplyGameEffects();
            }
            RecipeExecutionBuffer.ApplyDecorativeEffects();
        }

        public static string TimerAspectId(string elementId, string timerId)
        {
            return $"{elementId}.timers.{timerId}";
        }

        public static string TimerRepeatId(string elementId, string timerId)
        {
            return $"{elementId}.timersrepeat.{timerId}";
        }
    }
}
